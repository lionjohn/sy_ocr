#pragma once

#include <QtWidgets/QWidget>
#include "ui_sy_ocr.h"
#include <QSystemTrayIcon>

class IPicCodec;
class UrlInputWidget;
class QSystemTrayIcon;
class QLocalServer;
class sy_ocr : public QWidget
{
	Q_OBJECT

public:
	sy_ocr(QWidget *parent = Q_NULLPTR);
	~sy_ocr();
	//处理拖入程序或者是ctrl+v的图片
	void slotPasteLayer(const QMimeData* mimeData = nullptr);
	//处理返回结果
	void showRet(const QString &ret, const QString&);

public slots:
	//url图片解析入口
	void SlotInputUrl(QString);
	//内存图片解析入口
	void DecodeImage(const QImage&);
	//根据文件名加载图片
	void LoadImageFile(const QString&);
	//选择图片
	void SlotChooseFile();
	//跨进程管道通信
	void SlotNewConnection();
	void readyRead();
	//引擎菜单切换
	void SlotCodecChanged(QAction* act);
	//系统托盘菜单
	void SlotActiveTray(QSystemTrayIcon::ActivationReason);

	//关于窗体
	void SlotAbout();
protected:
	//拖放事件处理
	void dragEnterEvent(QDragEnterEvent *event) Q_DECL_OVERRIDE;
	void dragMoveEvent(QDragMoveEvent *event) Q_DECL_OVERRIDE;
	void dropEvent(QDropEvent *event) Q_DECL_OVERRIDE;
	//粘贴事件处理
	void keyPressEvent(QKeyEvent *) Q_DECL_OVERRIDE;
	//画背景
	void paintEvent(QPaintEvent *);
	//程序窗体拖动
	virtual void mousePressEvent(QMouseEvent *event);
	virtual void mouseMoveEvent(QMouseEvent *event);
	virtual void mouseReleaseEvent(QMouseEvent *event);
	//右键菜单
	void contextMenuEvent(QContextMenuEvent *) Q_DECL_OVERRIDE;

	//void closeEvent(QCloseEvent *);
private:

	Ui::sy_ocrClass ui;
	IPicCodec *codec_;
	UrlInputWidget *url_input_;

	QPixmap background_;

	bool is_drag_;
	QPoint drag_position_;
	QMenu *codec_select_menu_;
	QMenu *extend_setting_menu_;
	QAction *about_action_;
	QAction *exit_action_;
	//系统托盘
	QSystemTrayIcon *system_tray_;
	//进程通信
	QLocalServer *local_server_;
	//全局热键
	int hotkey_id_;
};
