v0.0.8 		2017-12-06 18:22:59 
1.增加系统托盘.
2.增加系统消息通知.
3.增加全局热键处理.现在可以通用ctrl+c->ctrl+alt+y->ctrl+v来完全快捷键操作.
4.增加拖动待识别图片到程序图标的处理.

v0.0.9 		2017年12月14日 18:35:50
1.增加关于信息.
2.程序主体的关闭按钮变成转到后台,点击后程序以托盘方式运行.右键和托盘上的退出才是真正的退出.

v0.0.10 	2018-01-12 15:32:13
1.百度的token可以通过配置文件-settings.ini修改.(感谢网友Liu Joey的建议)


v0.1.0     2018-03-08 17:16:51
1.加入离线识别支持.(要求大于win8.1)
2.百度token可以通过右键来更新.

v1.1.0   2018-03-09 11:57:06
1.就是这么任性,这是1.0版本了.
2.软件会自动识别系统版本,从而决定是否启动离线识别功能.