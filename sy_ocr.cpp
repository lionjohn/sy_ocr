#include "sy_ocr.h"
#include <QDrag>
#include <QEvent>
#include <QDragEnterEvent>
#include <QMimeData>
#include <QApplication>
#include <QClipboard>
#include <QLabel>
#include "sy_codec_factory.h"
#include <QMessageBox>
#include <QImageReader>
#include <QPainter>
#include <QMenu>
#include <QTextStream>
#include <QTextCodec>
#include <QLocalServer>
#include <QLocalSocket>
#include "urlInput/sy_url_input_dialog.h"
#include "shortcutKey/MyWinEventFilter.h"
#include "about/sy_about_widget.h"

bool VerifyPicUrls(QList<QUrl> urls)
{
	QString suffix1(".png"), suffix2(".bmp"), suffix3(".jpg"), suffix4(".jpeg");
	bool ok = true;
	for (auto url : urls)
	{
		QString filename = url.toLocalFile();
		if (filename.endsWith(suffix1, Qt::CaseInsensitive) ||
			filename.endsWith(suffix2, Qt::CaseInsensitive) ||
			filename.endsWith(suffix3, Qt::CaseInsensitive) ||
			filename.endsWith(suffix4, Qt::CaseInsensitive))
		{
		}
		else
		{
			ok = false;
			break;
		}
	}
	return ok;
}

//////////////////////////////////////////////////////////////////////////

sy_ocr::sy_ocr(QWidget *parent)
	: QWidget(parent), codec_(nullptr)
{
	ui.setupUi(this);
	setAcceptDrops(true);

	this->setWindowFlags(Qt::FramelessWindowHint); //设置无边框风格  
	this->setAttribute(Qt::WA_TranslucentBackground); //设置背景透明，允许鼠标穿透  

	sy_codec_register_all();
	//codec_ = PicCodecFactory::CreateCodec(CODEC_T_ONLINE_BAIDU);
	//codec_ = g_codec_list.at(CODEC_T_ONLINE_BAIDU);

	codec_select_menu_ = new QMenu(QStringLiteral("切换识别引擎"),this);
	extend_setting_menu_ = new QMenu(QStringLiteral("引擎设置"), this);
	QAction *default_act = nullptr;
	//用来保证互斥
	QActionGroup *act_group = new QActionGroup(codec_select_menu_);
	for (auto codec : g_codec_list)
	{
		if (codec)
		{
			auto act = new QAction(codec->GetCodecName());
			act->setCheckable(true);
			act->setData(QVariant::fromValue<IPicCodec*>(codec));
			codec_select_menu_->addAction(act);
			act_group->addAction(act);
			if (!default_act)
			{
				default_act = act;
			}
		};
	}
	connect(act_group, &QActionGroup::triggered, this, &sy_ocr::SlotCodecChanged);

	if (default_act)
	{
		//codec_select_menu_->setActiveAction(default_act);
		default_act->trigger();
	}

	bool dd = background_.load(":/sy_ocr/Resources/yiyi.png");
	is_drag_ = false;

	setToolTip(QStringLiteral("拖放或粘贴图片到此区域"));


	QString qssPath = QApplication::applicationDirPath() + "/qss/default.qss";
	QFile qssFile(qssPath);
	if (qssFile.open(QIODevice::ReadOnly))
	{
		QTextStream in(&qssFile);
		in.setCodec(QTextCodec::codecForLocale());
		QString styleSheet = in.readAll();
		this->setStyleSheet(styleSheet);
		qssFile.close();
	}

	connect(ui.CloseButton, &QPushButton::clicked, this, &QWidget::close);
	ui.CloseButton->setToolTip(QStringLiteral("退到后台"));

	url_input_ = new UrlInputWidget(this);
	connect(url_input_, &UrlInputWidget::SignalUrl, this, &sy_ocr::SlotInputUrl);

	//
	//系统托盘
	auto reshow = new QAction(QStringLiteral("显示主窗体"), this);
	exit_action_ = new QAction(QStringLiteral("退出"), this);
	about_action_ = new QAction(QStringLiteral("关于"), this);
	connect(exit_action_, &QAction::triggered, qApp, &QApplication::quit);
	connect(about_action_, &QAction::triggered, this, &sy_ocr::SlotAbout);
	connect(reshow, &QAction::triggered, this, &sy_ocr::show);

	auto myMenu = new QMenu(this);
	myMenu->addAction(reshow);
	myMenu->addSeparator();
	myMenu->addMenu(codec_select_menu_);
	myMenu->addMenu(extend_setting_menu_);
	myMenu->addSeparator();
	myMenu->addAction(about_action_);
	myMenu->addAction(exit_action_);
	//判断系统是否支持托盘图标  
	Q_ASSERT(QSystemTrayIcon::isSystemTrayAvailable());
	system_tray_ = new QSystemTrayIcon(this);
	system_tray_->setIcon(QIcon(":/sy_ocr/Resources/yiyi.png"));
	system_tray_->setToolTip(QStringLiteral("sy_OCR文字识别工具"));
	system_tray_->setContextMenu(myMenu);
	system_tray_->show();

	connect(system_tray_, &QSystemTrayIcon::activated, this, &sy_ocr::SlotActiveTray);//点击托盘，执行相应的动作  

	//管道通信
	local_server_ = new QLocalServer;
	QLocalServer::removeServer("sy_ocr");
	local_server_->listen("sy_ocr");
	connect(local_server_, SIGNAL(newConnection()), this, SLOT(SlotNewConnection()));
	//
	//全局热键
	hotkey_id_ = GlobalAddAtom(L"sy_ocr") - 0xC000;
	auto m_filter = new MyWinEventFilter(this);
	qApp->installNativeEventFilter(m_filter);
	if (!RegisterHotKey(0, hotkey_id_, MOD_CONTROL | MOD_ALT, 'Y'))
	{
		QMessageBox::warning(this, QStringLiteral("全局热键注册失败"), QStringLiteral("Ctrl+Alt+Y 已经被其它程序占用!"), QMessageBox::Yes, QMessageBox::Yes);
	}


}

sy_ocr::~sy_ocr()
{
	UnregisterHotKey(0, hotkey_id_);
	delete local_server_;
	delete system_tray_;

	delete url_input_;

	//暂时放这里处理
	for (auto codec : g_codec_list)
	{
		if (codec)
		{
			delete codec;
		}
	}//winrt有什么东西没有安全退出.先不管了.
}

void sy_ocr::SlotCodecChanged(QAction* act)
{
	if (!act)
		return;

	if (codec_)
	{
		disconnect(codec_, 0, this, 0);
		codec_ = nullptr;
	}

	auto codec = act->data().value<IPicCodec*>();
	codec_ = codec;
	connect(codec_, &IPicCodec::SignalDecodeReturn, this, &sy_ocr::showRet, Qt::QueuedConnection);

	auto extend_acts = codec_->GetCapacity();
	extend_setting_menu_->clear();
	for (auto act : extend_acts)
	{
		extend_setting_menu_->addAction(act);
	}
}

void sy_ocr::SlotActiveTray(QSystemTrayIcon::ActivationReason reason)
{
	if (reason == QSystemTrayIcon::DoubleClick)
	{
		this->show();
	}
}

void sy_ocr::SlotNewConnection()
{
	QLocalSocket *newsocket = local_server_->nextPendingConnection();
	connect(newsocket, SIGNAL(readyRead()), this, SLOT(readyRead()));
	connect(newsocket, &QLocalSocket::disconnected, newsocket,&QLocalSocket::deleteLater);
}

void sy_ocr::readyRead()
{
	// 取得是哪个localsocket可以读数据了
	QLocalSocket *local = static_cast<QLocalSocket *>(sender());
	if (!local)
		return;
	QTextStream in(local);
	QString     readMsg;
	// 读出数据
	readMsg = in.readAll();
	// 发送收到数据信号
	LoadImageFile(readMsg);
}

void sy_ocr::paintEvent(QPaintEvent *)
{
	QPainter p(this);
	p.drawPixmap(0, 0, background_);
}

void sy_ocr::dragEnterEvent(QDragEnterEvent *event)
{
	if (event->mimeData()->hasUrls())
	{//支持外部图片拖放
		auto urls = event->mimeData()->urls();
		if (VerifyPicUrls(urls))
		{
			event->acceptProposedAction();
		}
		else
		{
			event->ignore();
		}
	}
	else {
		event->ignore();
	}
}


void sy_ocr::dragMoveEvent(QDragMoveEvent *event)
{
	event->acceptProposedAction();
}

void sy_ocr::dropEvent(QDropEvent *event)
{
	if (event->mimeData()->hasUrls())
	{//支持外部图片拖放
		slotPasteLayer(event->mimeData());
		event->accept();
	}
	else {
		event->ignore();
	}
}

void sy_ocr::keyPressEvent(QKeyEvent *pEvent)
{
	if (pEvent->modifiers() == Qt::ControlModifier && pEvent->key() == Qt::Key_V)
	{
		slotPasteLayer(nullptr);
		return;
	}
}


void sy_ocr::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		is_drag_ = true;
		drag_position_ = event->globalPos() - this->pos();
		event->accept();
	}
}

void sy_ocr::mouseMoveEvent(QMouseEvent *event)
{
	if (is_drag_ && (event->buttons() && Qt::LeftButton))
	{
		move(event->globalPos() - drag_position_);
		event->accept();
	}
}

void sy_ocr::mouseReleaseEvent(QMouseEvent *event)
{
	is_drag_ = false;
}

void sy_ocr::SlotChooseFile()
{
	QFileDialog stFileDialog(this, QString::fromLocal8Bit("打开图片文件"), NULL, QString::fromLocal8Bit("图片文件(*.png; *.jpg; *.bmp)"));
	stFileDialog.setFileMode(QFileDialog::ExistingFile);

	if (stFileDialog.exec() == QDialog::Accepted)
	{
		LoadImageFile(stFileDialog.selectedFiles().first());
	}
}

void sy_ocr::contextMenuEvent(QContextMenuEvent *pEvent)
{
	auto Context = new QMenu(this);

	auto openFileAction = new QAction(QStringLiteral("打开图片文件"), Context);
	auto openUrlAction = new QAction(QStringLiteral("打开URL"), Context);

	Context->addAction(openFileAction);
	Context->addAction(openUrlAction);
	Context->addSeparator();

	Context->addMenu(codec_select_menu_);
	Context->addMenu(extend_setting_menu_);
	Context->addSeparator();
	Context->addAction(about_action_);
	Context->addAction(exit_action_);

	connect(openFileAction, &QAction::triggered, this, &sy_ocr::SlotChooseFile);
	connect(openUrlAction, &QAction::triggered, url_input_, &QDialog::exec);


	Context->exec(pEvent->globalPos());
}

void sy_ocr::SlotAbout()
{
	SYAbout about;
	about.exec();
}

void sy_ocr::SlotInputUrl(QString url)
{
	//bool is_true = codec_->DecodeURL(url, std::bind(&sy_ocr::showRet, this/*调用者*/, std::placeholders::_1/*参数1*/, std::placeholders::_2/*参数2*/));
	int status = codec_->DecodeURL(url);
	if (status < 0)
	{
		QMessageBox::warning(this, QStringLiteral("失败"), QStringLiteral("正在解析中,请稍后再试..."), QMessageBox::Yes, QMessageBox::Yes);
	}
	else if (status == 1)
	{
		system_tray_->showMessage(QStringLiteral("开始识别"), QStringLiteral("识别进行中..."), QSystemTrayIcon::Information, 1000);
	}
}

void sy_ocr::LoadImageFile(const QString& filename)
{
	QImage in;
	if (in.load(filename))
	{
		DecodeImage(in);
	}
	else
	{
		QMessageBox::warning(this, QStringLiteral("失败"), QStringLiteral("文件格式不支持"), QMessageBox::Yes, QMessageBox::Yes);
	}
}

void sy_ocr::DecodeImage(const QImage& in)
{
	int status = codec_->DecodeImage(in);
	if (status < 0)
	{
		QMessageBox::warning(this, QStringLiteral("失败"), QStringLiteral("正在解析中,请稍后再试..."), QMessageBox::Yes, QMessageBox::Yes);
	}
	else if (status == 1)
	{
		system_tray_->showMessage(QStringLiteral("开始识别"), QStringLiteral("识别进行中..."), QSystemTrayIcon::Information, 1000);
	}
}

void sy_ocr::slotPasteLayer(const QMimeData* mimeData)
{
	if (!mimeData)
	{
		mimeData = QApplication::clipboard()->mimeData();
	}
	auto urls = mimeData->urls();
	if (urls.size() > 0)
	{
		LoadImageFile(urls[0].toLocalFile());
	}
	else if (mimeData->hasImage())
	{
		QImage stImage = qvariant_cast<QImage>(mimeData->imageData());
		DecodeImage(stImage);
	}
	else
	{
		return;
	}


}

void sy_ocr::showRet(const QString &ret, const QString&errStr)
{
	QString title, body;
	if (ret == "")
	{
		title = QStringLiteral("失败");
		body = errStr;
	}
	else
	{
		QApplication::clipboard()->clear();
		QApplication::clipboard()->setText(ret);
		//QMessageBox::about(this, QStringLiteral("内容已经复制到剪切板!"), ret);
		title = QStringLiteral("识别完成");
		body = QStringLiteral("文本已经复制到剪切板!");
	}
	system_tray_->showMessage(title, body, QSystemTrayIcon::Information, 2000);
}

