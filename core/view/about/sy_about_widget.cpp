#include "sy_about_widget.h"
#include <QLayout>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>

SYAbout::SYAbout(QWidget *parent) : QDialog(parent)
{
	setFixedSize(340, 160);

	Qt::WindowFlags flags = Qt::Dialog;
	flags |= Qt::WindowCloseButtonHint;
	setWindowFlags(flags);

	auto oldFont = font();
	oldFont.setPointSize(oldFont.pointSize() + 2);
	setFont(oldFont);

	QVBoxLayout *layout = new QVBoxLayout(this);
	//layout->setMargin(0);
	layout->setContentsMargins(6, 10, 6, 6);
	layout->setSpacing(0);
	layout->addStretch();
	QString aboutStr = QStringLiteral("  欢迎使用本软件!  当前版本v1.1.0(2018.03.09)<br/>") +
		QStringLiteral("如果你有任何意见或者想法,都可以联系本软件作者(xiao17174@126.com)!<br/><br/>") +
		QStringLiteral("本软件基于GPL-V2协议开放源码,源码地址:<a href = https://gitee.com/xiao17174/sy_ocr>https://gitee.com/xiao17174/sy_ocr</a>(点击访问) \n");
	auto lable = new QLabel(this);
	lable->setTextFormat(Qt::TextFormat::RichText);
	lable->setWordWrap(true);
	lable->setOpenExternalLinks(true);
	lable->setText(aboutStr);
	layout->addWidget(lable);
	layout->addStretch();

	QHBoxLayout *hlayout = new QHBoxLayout(this);
	hlayout->addStretch();
	QPushButton *ok = new QPushButton(QStringLiteral("确定"),this);
	connect(ok, &QPushButton::clicked, this, &QDialog::close);
	hlayout->addWidget(ok);

	layout->addLayout(hlayout);

	setLayout(layout);

	setWindowTitle(QStringLiteral("关于本软件"));
}

SYAbout::~SYAbout()
{

}