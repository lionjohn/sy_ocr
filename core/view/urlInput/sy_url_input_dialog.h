#pragma once
#include <QtWidgets>

class UrlInputWidget : public QDialog
{
	Q_OBJECT
public:
	UrlInputWidget(QWidget* parent = nullptr);
	~UrlInputWidget();

signals:
	void SignalUrl(QString);

public slots:
	void SlotUrlCommit();
protected:
	void showEvent(QShowEvent *);
private:
	QLineEdit *url_edit_;
	QPushButton *commit_button_;
};