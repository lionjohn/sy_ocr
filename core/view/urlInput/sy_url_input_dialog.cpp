#include "sy_url_input_dialog.h"

UrlInputWidget::UrlInputWidget(QWidget* parent /* = nullptr */) : QDialog(parent)
{
	Qt::WindowFlags flags = Qt::Dialog;
	flags |= Qt::WindowCloseButtonHint;
	setWindowFlags(flags);
	setWindowTitle(QStringLiteral("请输入图片网址(不支持https)"));
	setMinimumWidth(300);
	QHBoxLayout *backLayout = new QHBoxLayout();

	url_edit_ = new QLineEdit;
	backLayout->addWidget(url_edit_);

	commit_button_ = new QPushButton(QStringLiteral("提交"));
	backLayout->addWidget(commit_button_);
	commit_button_->setMaximumWidth(40);
	setLayout(backLayout);

	connect(commit_button_, &QPushButton::clicked, this, &UrlInputWidget::SlotUrlCommit);
}

UrlInputWidget::~UrlInputWidget()
{

}

void UrlInputWidget::showEvent(QShowEvent *)
{
	url_edit_->clear();
}

void UrlInputWidget::SlotUrlCommit()
{
	if (url_edit_->text() != "")
	{
		emit SignalUrl(url_edit_->text());
	}
	this->hide();
}