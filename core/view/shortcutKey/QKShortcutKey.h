/**************************************************************************
* Copyright (c) 2017,QuKan
*
* 作    者： Qjj
* 创建日期： 2017/11/02
* 摘    要：
*
***************************************************************************/
#ifndef _2017_11_02_13_43_40_H_
#define _2017_11_02_13_43_40_H_
#include <windows.h>
#include <functional>
#include <QObject>
#include "helper/UtilityMarco.h"

typedef std::function<void()> DoFunc;

class MyWinEventFilter;
class QKShortcutKey : public QObject
{
	IS_SINGLETON( QKShortcutKey )
public:
	void addFunc( QKeySequence keySequence, DoFunc doFunc );
	bool ExecuteFunc( QKeySequence keySequence );
	void StartShortcut();

private:
	static quint32 nativeKeycode( Qt::Key keycode );
	static quint32 nativeModifiers( Qt::KeyboardModifiers modifiers );
	bool registerHotKey();
	bool unregisterHotKey();

private:
	std::map<QKeySequence, DoFunc> m_funcs;
	MyWinEventFilter *m_filter;
	Qt::Key m_key;
	Qt::KeyboardModifiers m_mods;
	QKeySequence m_keySequence;
	bool start_shortcut_;
	
};

#endif