#pragma once
#include <QAbstractNativeEventFilter>
#include <QObject>
#include <windows.h>
class QKShortcutKey;
class sy_ocr;
class MyWinEventFilter : public QAbstractNativeEventFilter
{
public:
	MyWinEventFilter(sy_ocr*);
	~MyWinEventFilter();

	void SetHotKey();

	virtual bool nativeEventFilter(const QByteArray &eventType, void *message, long *);

	quint32 static nativeModifiers(Qt::KeyboardModifiers modifiers);
	quint32 static nativeKeycode(Qt::Key key);
signals:
	void SignalHotKey();
private:


	quint32 cur_hotkey_;
	sy_ocr *dady_;
};