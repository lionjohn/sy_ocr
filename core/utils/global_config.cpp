#include "global_config.h"


SY_Global::SY_Global()
{
	QString appDir = qApp->applicationDirPath();
	QString config_file = appDir + "/settings.ini";
	config_ = new QSettings(config_file, QSettings::IniFormat);
}

SY_Global::~SY_Global()
{

}

QVariant SY_Global::GetIniValue(QString key)
{
	return config_->value(key);
}

void SY_Global::SetValue(QString key, QVariant value)
{
	config_->setValue(key, value);
}