#pragma once
#include <QtCore>


class SY_Global : public QObject
{
	Q_OBJECT
public:
	SY_Global();
	~SY_Global();
public:
	//the key maybe "native/baidu_token" or "ip"(global)
	QVariant GetIniValue(QString);
	void SetValue(QString, QVariant);
private:
	QSettings *config_;
};


Q_GLOBAL_STATIC(SY_Global, IniFile)