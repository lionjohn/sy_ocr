#include "sy_codec_baidu.h"
#include "sy_network.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QBuffer>
#include "utils/global_config.h"

PicCodecBaidu::PicCodecBaidu(QObject *parent /* = nullptr */) : IPicCodec(QStringLiteral("百度在线识别"), parent)
{
	access_token_ = IniFile()->GetIniValue("Native/baidu_token").toString();
	//ResetAccessToken();
	//IniFile()->SetValue("Native/baidu_token", access_token_);
	auto reset_token = new QAction(QStringLiteral("重置百度token"),this);
	reset_token->setToolTip(QStringLiteral("重置不会立即生效,建议等5秒后再操作!"));
	reset_token->setStatusTip(QStringLiteral("重置不会立即生效,建议等5秒后再操作!"));
	connect(reset_token, &QAction::triggered, this, &PicCodecBaidu::ResetAccessToken);
	AddCapacity(reset_token);
}

PicCodecBaidu::~PicCodecBaidu()
{

}

int PicCodecBaidu::DecodeURL(QString in)
{
	if (!SetWorkingMark(true))
		return -1;

	QByteArray qba("url=");
	qba.append(in);

	Decode(qba);
	return 1;
}

int PicCodecBaidu::DecodeImage(QImage in)
{
	if (!SetWorkingMark(true))
		return -1;

	QByteArray qba;
	QBuffer qbf(&qba);
	qbf.open(QIODevice::WriteOnly);
	auto ret = in.save(&qbf, "png");
	qbf.close();
	auto base64 = qba.toBase64();
	auto total = QByteArray("image=") + base64.toPercentEncoding();

	Decode(total);
	return 1;

}

bool PicCodecBaidu::Decode(const QByteArray& body)
{
	QString urlStr = "https://aip.baidubce.com/rest/2.0/ocr/v1/webimage?access_token=";
	urlStr += access_token_;
	QUrl url(urlStr);

	QNetworkRequest request;
	request.setUrl(url);
	request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

	auto reply = MynetworkManager::instance()->post(request, body);


	connect(reply, &QNetworkReply::readyRead, this, &PicCodecBaidu::SlotDecode);
	connect(reply, static_cast<void(QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error), this, &PicCodecBaidu::SlotDecode);
	connect(reply, &QNetworkReply::sslErrors, this, &PicCodecBaidu::SlotDecode);
	return true;
}

void PicCodecBaidu::SlotDecode()
{
	auto Sender = sender();

	QNetworkReply *reply = dynamic_cast<QNetworkReply *>(Sender);

	auto responseData = reply->readAll();
	reply->deleteLater();

	QString ret_srt, err_str;
	if (reply->error() != QNetworkReply::NoError)
	{
		err_str = reply->errorString();
	}
	else
	{
		QJsonParseError *err = nullptr;
		QJsonDocument jsonDoc = QJsonDocument::fromJson(responseData, err);
		auto jsonObject = jsonDoc.object();
		auto words = jsonObject["words_result"].toArray();

		QString retStr;
		for (auto word : words)
		{
			retStr.append(word.toObject()["words"].toString());
			retStr.append("\n");
		}
		ret_srt = retStr;
		//callback_(retStr,"");
	}
	//callback_ = nullptr;
	emit SignalDecodeReturn(ret_srt, err_str);
	SetWorkingMark(false);
	return;
}

void PicCodecBaidu::ResetAccessToken()
{
	QString urlStr = "https://aip.baidubce.com/oauth/2.0/token";
	urlStr += "?grant_type=client_credentials&client_id=xxx&client_secret=xxx&";
	QUrl url(urlStr);

	QNetworkRequest request;

	request.setUrl(url);
// 	QSslConfiguration config;
// 	config.setPeerVerifyMode(QSslSocket::VerifyNone);
// 	config.setProtocol(QSsl::TlsV1SslV3);
// 	request.setSslConfiguration(config);

	auto reply = MynetworkManager::instance()->post(request,(QIODevice*)nullptr);


	connect(reply, &QNetworkReply::readyRead, this, &PicCodecBaidu::SlotAccessToken);
	connect(reply, static_cast<void(QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error), this, &PicCodecBaidu::SlotAccessToken);
	connect(reply, &QNetworkReply::sslErrors, this, &PicCodecBaidu::SlotAccessToken);
	return;
}

void PicCodecBaidu::SlotAccessToken()
{
	auto Sender = sender();

	QNetworkReply *reply = dynamic_cast<QNetworkReply *>(Sender);

	auto errorstr = reply->errorString();
	auto responseData = reply->readAll();
	reply->deleteLater();
	if (reply->error() != QNetworkReply::NoError)
	{
		return;
	}

	QJsonParseError *err = nullptr;
	QJsonDocument jsonDoc = QJsonDocument::fromJson(responseData, err);
	auto jsonObject = jsonDoc.object();
	access_token_ = jsonObject["access_token"].toString();
	IniFile()->SetValue("Native/baidu_token", access_token_);
}