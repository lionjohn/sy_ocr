#pragma once
#include <QNetworkAccessManager>
#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>

class MynetworkManager : public QNetworkAccessManager
{
	Q_OBJECT
public:
	// 获取静态全局变量
	static MynetworkManager* instance();

public:
	MynetworkManager(QObject *parent = nullptr);
	~MynetworkManager();

private:
	static MynetworkManager *g_HttpManager;
};