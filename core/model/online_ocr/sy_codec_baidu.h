#pragma once
#include "core/model/sy_codec.h"
#include <QMap>

class PicCodecBaidu : public IPicCodec
{
	Q_OBJECT
public:
	PicCodecBaidu(QObject *parent = nullptr);
	~PicCodecBaidu();

public:
	bool Init() override;

	int DecodeImage(QImage) override;
	int DecodeURL(QString) override;

	void ResetAccessToken();

	public slots:
	void SlotAccessToken();
	void SlotDecode();
private:
	bool Decode(const QByteArray&);

	QString access_token_;
};
