#include "sy_codec_factory.h"
#include "online_ocr/sy_codec_baidu.h"
#include "win10_native_ocr/sy_codec_uwp_api.h"

QList<IPicCodec *> g_codec_list;
// IPicCodec* PicCodecFactory::CreateCodec(CodecType type)
// {
// 	IPicCodec* tmp = nullptr;
// 	switch (type)
// 	{
// 	case CODEC_T_DEFAULT:
// 	case CODEC_T_ONLINE_BAIDU:
// 		tmp = new PicCodecBaidu();
// 		break;
// 	case CODEC_T_UWP_API:
// 		tmp = new PicCodecUWP();
// 		break;
// 	default:
// 		break;
// 	}
// 	return tmp;
// }


void sy_codec_register_all()
{
	g_codec_list.push_back(new PicCodecBaidu());
	g_codec_list.push_back(new PicCodecUWP());

	for (auto iter = g_codec_list.begin();iter != g_codec_list.end();iter++)
	{
		if (!(*iter)->Init())
		{
			delete *iter;
			*iter = nullptr;
		}
	}
}