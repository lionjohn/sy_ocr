#pragma once
#include "sy_codec.h"
#include "online_ocr/sy_codec_baidu.h"
#include "win10_native_ocr/sy_codec_uwp_api.h"

enum CodecType
{
	CODEC_T_ONLINE_BAIDU = 0,
	CODEC_T_UWP_API = 1,
};

// class PicCodecFactory
// {
// public:
// 	static IPicCodec *CreateCodec(CodecType);
// };

extern QList<IPicCodec *> g_codec_list;

void sy_codec_register_all();