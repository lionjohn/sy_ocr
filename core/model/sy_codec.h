#pragma once
#include <QObject>
#include <QImage>
#include <functional>
#include <QAction>
#include <QString>


class IPicCodec : public QObject
{
	Q_OBJECT
public:
	IPicCodec(QString name,QObject *parent = nullptr) : QObject(parent), codec_name_(name) {};
	virtual ~IPicCodec() {};
public:
	//最好要考虑多次调用
	virtual bool Init() { return true; };
	virtual void Uninit() {};

	virtual int DecodeImage(QImage) = 0;
	virtual int DecodeURL(QString) = 0;

	virtual QList<QAction*> GetCapacity() { return actions_; };
	virtual void AddCapacity(QAction*action) { actions_.append(action); };
	bool SetWorkingMark(bool b) { if (b == is_working) return false; is_working = b; return true; };
	bool WorkingMark() { return is_working; };
	QString GetCodecName() { return codec_name_; };
signals:
	void SignalDecodeReturn(const QString&,const QString&);

private:
	bool is_working = false;
	QList<QAction*> actions_;
	QString codec_name_;
};