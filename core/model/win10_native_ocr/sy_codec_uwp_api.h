#pragma once
#include "winrt/Windows.Storage.Streams.h"
#include "winrt/Windows.Graphics.Imaging.h"
#include "winrt/Windows.Media.Ocr.h"

#include "core/model/sy_codec.h"
#include "sy_uwp_thread.h"
#include "winrt/Windows.Globalization.h"

class QAction;
class PicCodecUWP :public IPicCodec
{
	Q_OBJECT
public:
	PicCodecUWP(QObject *parent = nullptr) ;
	~PicCodecUWP();

public:
	bool Init() override;
	void Uninit() override;


	int DecodeImage(QImage) override;
	int DecodeURL(QString) override;

	public slots:
	 void SlotResult(QStringList);
	 void SlotLangChanged(QAction*);
private:
	void BufferAlloc(int size);

	uint8_t *nopadding_buffer_ = nullptr;
	uint8_t buffer_size_ = 0;

	UWPWorker *worker_;

	//QActionGroup *action_group_;
};

//Q_DECLARE_METATYPE(winrt::Windows::Media::Ocr::OcrResult)