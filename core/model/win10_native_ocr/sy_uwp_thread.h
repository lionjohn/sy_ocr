#pragma once
#include <QObject>
#include <thread>
#include <condition_variable>
#include <memory>
#include <QWaitCondition>
#include <QMutex>
#pragma comment(lib, "windowsapp")
#pragma comment(lib, "pathcch")
#include "winrt/Windows.Foundation.h"
#include "winrt/Windows.Storage.Streams.h"
#include "winrt/Windows.Graphics.Imaging.h"
#include "winrt/Windows.Media.Ocr.h"


//简易线程基类.来自网友http://blog.csdn.net/unclerunning/article/details/72036805
class SimpleThread
{
public:
	SimpleThread() :m_finished(false),
		m_isStart(false)
	{

	}

	void start() {
		m_thd = std::make_unique<std::thread>(&SimpleThread::entry, this);
		m_tid = m_thd->get_id();

		m_thd->detach(); // 分离线程
		m_isStart = true;
	}

	bool wait(std::chrono::milliseconds waitTime)
	{
		bool ret = true;

		if (!m_isStart)
			return ret;

		std::unique_lock<std::mutex> lock(m_cvM);
		while (!m_finished)
		{
			if (m_cv.wait_for(lock, waitTime) == std::cv_status::timeout)
			{
				ret = false;
				break;
			}
		}

		return ret;
	}

	void wait()
	{
		if (!m_isStart)
			return;

		std::unique_lock<std::mutex> lock(m_cvM);
		while (!m_finished)
			m_cv.wait(lock);
	}

	std::thread::id getTid() { return m_tid; }

	// 虚函数，子类实现
	virtual void run() = 0;

protected:
	SimpleThread(const SimpleThread&) = delete;
	SimpleThread& operator =(const SimpleThread&) = delete;
private:
	void entry() {

		// 真正执行的是一个动态的虚函数
		this->run();

		m_isStart = false;
		// 线程结束
		std::unique_lock<std::mutex> lock(m_cvM);
		m_finished = true;
		lock.unlock();
		// 结束条件满足，唤醒所有等待该线程结束的线程
		m_cv.notify_all();
	}
private:
	std::mutex m_cvM;
	std::condition_variable m_cv;
	bool m_finished;

	bool m_isStart;
	std::thread::id m_tid;
	std::unique_ptr<std::thread> m_thd;
};

/*
以C++风格形式创建线程,去运行WinRT的接口.为了可以正确的CoInitialize(因为QApplication也会初始化这个,然而初始化的参数不同)
如果强行在主线程CoInitialize,会导致QT很多功能不正常.例如QDrag等
*/
class UWPWorker : public QObject,public SimpleThread
{
	Q_OBJECT
public:
	UWPWorker();
	UWPWorker(winrt::Windows::Globalization::Language);
	~UWPWorker();

public:
	void run();
	void stop();
	void RecognizeBitmap(const winrt::Windows::Graphics::Imaging::SoftwareBitmap);

	void SetOcrEngineLang(winrt::Windows::Globalization::Language);
	winrt::Windows::Foundation::IAsyncOperation<winrt::Windows::Media::Ocr::OcrResult> AsyncOperate(const winrt::Windows::Graphics::Imaging::SoftwareBitmap bitmap);

	bool HaveEngine();
signals:
	void SignalReslut(QStringList);

private:
	std::mutex mutex_;
	std::condition_variable wait_condition_;
	winrt::Windows::Graphics::Imaging::SoftwareBitmap bitmap_;
	bool stop_ = false;
	winrt::Windows::Globalization::Language lang_;
	winrt::Windows::Media::Ocr::OcrEngine engine_;
};