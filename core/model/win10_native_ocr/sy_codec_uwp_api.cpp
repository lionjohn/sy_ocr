#include "sy_codec_uwp_api.h"
#include <qmenu.h>

using namespace winrt;
using namespace Windows::Foundation;
using namespace Windows::Storage;
using namespace Windows::Storage::Streams;
using namespace Windows::Graphics::Imaging;
using namespace Windows::Media::Ocr;
using namespace std::chrono;
using namespace Windows::Globalization;

PicCodecUWP::PicCodecUWP(QObject *parent /* = nullptr */) : IPicCodec(QStringLiteral("win10内置识别"),parent)
, worker_(nullptr)
{
	//构建右键的进阶设置菜单
	auto lang_set = new QAction(QStringLiteral("切换首选语言"),this);
 	AddCapacity(lang_set);
}

PicCodecUWP::~PicCodecUWP()
{
	Uninit();
}

bool PicCodecUWP::Init()
{
	//先判断是否为win8.1或win10  
	typedef void(__stdcall*NTPROC)(DWORD*, DWORD*, DWORD*);
	auto hinst = LoadLibrary(L"ntdll.dll");
	DWORD dwMajor, dwMinor, dwBuildNumber;
	NTPROC proc = (NTPROC)GetProcAddress(hinst, "RtlGetNtVersionNumbers");
	proc(&dwMajor, &dwMinor, &dwBuildNumber);
	FreeLibrary(hinst);
	//dwMajor = 5;
	if (dwMajor < 6 || (dwMajor == 6 && dwMinor < 3))
	{
		return false;
	}
	//启动工作线程
	if (worker_)
	{
		return true;
	}
	worker_ = new UWPWorker;
	worker_->start();
	connect(worker_, &UWPWorker::SignalReslut, this, &PicCodecUWP::SlotResult, Qt::QueuedConnection);

	//填充右键菜单
	auto lang_set = GetCapacity().first();
	auto old_menu = lang_set->menu();
	if (old_menu)
	{
		return true;
	}
	auto langs = OcrEngine::AvailableRecognizerLanguages();
	QMenu *menu = new QMenu();
	auto action_group = new QActionGroup(lang_set);

	for (auto &lang : langs)
	{
		auto act = new QAction(QString::fromStdWString(lang.DisplayName().c_str()));
		act->setCheckable(true);

		act->setData(QString::fromStdWString(lang.LanguageTag().c_str()));
		menu->addAction(act);
		action_group->addAction(act);
	}
	lang_set->setMenu(menu);
	connect(action_group, &QActionGroup::triggered, this, &PicCodecUWP::SlotLangChanged);
	//设置首选语言
	auto cur_lang_tag = Windows::Globalization::Language::CurrentInputMethodLanguageTag();
	auto cur_lang_tag_str = QString::fromStdWString(cur_lang_tag.c_str());

	auto lists = action_group->actions();
	for (auto act : lists)
	{
		auto lang_str = act->data().toString();

		if (cur_lang_tag_str == lang_str)
		{
			act->trigger();
			return true;
		}
	}
	if (lists.size() > 0)
	{
		lists.first()->trigger();
	}
	return true;
}

void PicCodecUWP::Uninit()
{
	auto lang_set = GetCapacity().first();
	auto old_menu = lang_set->menu();
	if (old_menu)
	{
		lang_set->setMenu(nullptr);
		old_menu->deleteLater();
	}
	if (worker_)
	{
		worker_->stop();
		worker_->wait();
		delete worker_;
		worker_ = nullptr;
	}
}

void PicCodecUWP::SlotLangChanged(QAction *act)
{
	if (!act)
	{
		return;
	}

	auto lang_str = act->data().toString();
	Windows::Globalization::Language lang(lang_str.toStdWString());
	worker_->SetOcrEngineLang(lang);
}

void PicCodecUWP::BufferAlloc(int size)
{
	if (buffer_size_ < size)
	{
		if(nopadding_buffer_)
			free(nopadding_buffer_);
		nopadding_buffer_ = nullptr;
		buffer_size_ = 0;

		nopadding_buffer_ = (uint8_t*)malloc(size);
		buffer_size_ = size;
	}
}



int PicCodecUWP::DecodeImage(QImage in)
{
	if (!SetWorkingMark(true))
		return -1;

	if (!worker_->HaveEngine())
	{
		return -2;
	}

	int no_pad_size = in.width() * in.height() * 4;
	BufferAlloc(no_pad_size);

	if (no_pad_size == in.byteCount())
	{
		memcpy(nopadding_buffer_, in.constBits(), no_pad_size);
	}
	else
	{

		for (int i = 0;i < in.height();i++)
		{
			memcpy(nopadding_buffer_ + in.width() * 4 * i, in.constScanLine(i), in.width() * 4);
		}
	}
	//从qimage复制到SoftwareBitmap
	DataWriter writer;
	writer.WriteBytes(array_view<const uint8_t>(nopadding_buffer_, nopadding_buffer_ + no_pad_size));

	SoftwareBitmap bitmap = SoftwareBitmap::CreateCopyFromBuffer(writer.DetachBuffer(), BitmapPixelFormat::Rgba8, in.width(), in.height());


	//auto ret = AsyncOperate(bitmap).get();
	worker_->RecognizeBitmap(bitmap);
	return 0;
}

void PicCodecUWP ::SlotResult(QStringList retlist)
{
	QString retStr;
	for (auto &line : retlist)
	{
		retStr += line;
	}

	emit SignalDecodeReturn(retStr, "");
	SetWorkingMark(false);
	return;
}

int PicCodecUWP::DecodeURL(QString)
{
	return -1;
}